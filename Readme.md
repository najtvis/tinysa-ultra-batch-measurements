# TinySA Ultra - Batch Measurement

This script is intended for EMC measurements with LISN. It is used for pre-certification to determine whether or not the device exceeds the limits.

## Information
Script for automatic measurement for EMC pre-certification in the range of 150 kHz and 1 GHz with indicated limits according to CISPR32 Class A at 3m.

Due to some limitations of this measurement, this method is not 100% reliable, but it could indicate the problems with the board.

### Used Equipment:
   - TinySA Ultra
   - LISN (9 kHz - ~350 MHz) with attenuator of 10 dB and transient limiter
       - https://github.com/bvernoux/EMC_5uH_LISN
   - DM/CM separator

### Script is based on QtTinySA 
  - Used only USB reading part
  - Available at https://github.com/g4ixt/QtTinySA

## Arrangement

  - Place the tested device 5 cm above the conductive sheet and at least 10 cm from the edge of the conductive sheet.
  - Connect the LISN to the tested device and keep a distance of 20-40 cm between them.
  - Connect LISN and plate (GND terminal)
  - Connect LISN to spectrum analyser (to measure EMC, measure Common Mode part via DM/CM separator)

Some information on measurement and arrangement can be found at https://www.tekbox.com/product/LISN_Basics_and_Overview.pdf
