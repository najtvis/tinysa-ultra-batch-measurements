import time
import logging
import numpy as np
import queue
from platform import system
import struct
import serial
from serial.tools import list_ports


class USB_Device:
    
    def __init__(self):
        self.usb = None
        self.sweeping = False
        self.scale = 174
        self.scanMemory = 50
        self.scan3D = False
        self.surface = None
        self.vGrid = None
        self.fifo = queue.SimpleQueue()
        self.tinySA4 = None
        self.resBW = ['0.2', '1', '3', '10', '30', '100', '300', '600', '850']
        
        self.rbw = 10
        self.startF = 150e3
        self.endF = 1e6
        self.spanF = self.endF - self.startF
        self.points = 1000
            
    def openPort(self):
        self.dev = None
        # Get tinysa device (port) automatically using hardware ID
        VID = 0x0483  # 1155
        PID = 0x5740  # 22336
        device_list = list_ports.comports()
        for x in device_list:
            if x.vid == VID and x.pid == PID:
                self.dev = x.device
                logging.info(f'Found TinySA on {self.dev}')
        if self.dev and self.usb is None:  # TinySA was found but serial comms not open
            try:
                self.usb = serial.Serial(self.dev, baudrate=576000)
                logging.info(f'Serial port open: {self.usb.isOpen()}')
            except serial.SerialException:
                logging.info('serial port exception')
        if self.dev and self.usb:
            self.initialise()

    def closePort(self):
        if self.usb:
            self.usb.close()
            logging.info(f'Serial port open: {self.usb.isOpen()}')
            self.usb = None

    def isConnected(self):
        if self.dev is None:
            self.openPort()

    def initialise(self):
        i = 0
        hardware = ''
        while hardware[:6] != 'tinySA' and i < 3:  # try 3 times to detect TinySA
            hardware = self.version()
            logging.info(f'{hardware[:16]}')
            i += 1
            time.sleep(0.5)
        # hardware = 'tinySA'  # used for testing
        if hardware[:7] == 'tinySA4':  # It's an Ultra
            self.tinySA4 = True
        else:
            self.tinySA4 = False
            self.scale = 128
            self.resBW = self.resBW[2:8]  # TinySA Basic has fewer resolution bandwidth filters
        self.spur()

        # Basic has no lna
        self.lna(True)
        
        self.setRBW(self.rbw)

        # self.fifoTimer.start(500)  # calls self.usbSend() every 500mS to execute serial commands whilst not scanning
        
    def usbSend(self):
        try:
            self.usb.timeout = 1
        except AttributeError:  # don't know why this happens on second run of programme.  Temporary workaround.
            self.usb = serial.Serial(self.dev, baudrate=576000)
            logging.info(f'Serial port open: {self.usb.isOpen()}')
        while self.fifo.qsize() > 0:
            command = self.fifo.get(block=True, timeout=None)
            logging.debug(command)
            self.usb.write(command)
            self.usb.read_until(b'ch> ')  # skip command echo and prompt

    def serialQuery(self, command):
        self.usb.timeout = 1
        logging.debug(command)
        self.usb.write(command)
        self.usb.read_until(command + b'\n')  # skip command echo
        response = self.usb.read_until(b'ch> ')
        logging.debug(response)
        return response[:-6].decode()  # remove prompt
    
    
    # Commands
    
    def lna(self, lna):
        if lna:
            command = 'lna on\r'.encode()
        else:
            command = 'lna off\r'.encode()
        self.serialQuery(command)
    
    def setRBW(self, rbw):  # may be called by measurement thread as well as normally
        self.rbw = rbw
        logging.debug(f'rbw = {self.rbw}')
        command = f'rbw {self.rbw}\r'.encode()
        self.serialQuery(command)

    def setPoints(self):  # may be called by measurement thread as well as normally
        self.points = 3 * int(self.spanF/(self.rbw*1000))  # RBW multiplier * freq in kHz
        self.points = np.clip(self.points, 200, 30000)  # limit points
        logging.debug(f'setPoints: points = {self.points}')
        return self.points
    
    def set_frequencies(self, start, end):  # creates a numpy array of equi-spaced freqs in Hz. Also called by measurement thread.
        self.startF = start
        self.endF = end        
        self.spanF = self.endF - self.startF
        self.set_sweep()
        self.setPoints()
        frequencies = np.linspace(self.startF, self.endF, self.points, dtype=np.int64)
        logging.info(f'number of frequencies is {self.points}')
        # logging.debug(f'frequencies = {frequencies}')
        return frequencies
    
    def version(self):
        command = 'version\r'.encode()
        version = self.serialQuery(command)
        return version
    
    def spur(self):
        command = 'spur off\r'.encode()
        self.serialQuery(command)
        
    def set_sweep(self):
        command = f'sweep {self.startF} {self.endF}\r'.encode()
        self.serialQuery(command)
    
    def printProgress(self, number, total, prefix = ''):
        percent = ("{0:.1f}").format(100 * (number / float(total)))
        filledLength = int((50 * number)/ total)
        bar = '#' * filledLength + '-' * (50 - filledLength)
        print(f'\r{prefix} [{bar}] {percent}%  ', end = "\r")
        
    # measurements
    def measurement(self, frequencies, readings, maxCount):  # runs in a separate thread
        points = np.size(readings, 1)
        scanID = 0
        self.sweeping = 1
        while self.sweeping:
            try:
                self.usb.timeout = self.sweepTimeout()
                command = f'scanraw {int(frequencies[0])} {int(frequencies[-1])} {int(points)}\r'.encode()
                self.usb.write(command)
                index = 0
                # self.runTimer.start()  # debug
                self.usb.read_until(command + b'\n{')  # skip command echo
                dataBlock = ''
                while dataBlock != b'}ch' and index < points:  # if '}ch' it's reached the end of the scan points
                    dataBlock = (self.usb.read(3))  # read a block of 3 bytes of data
                    logging.debug(f'dataBlock: {dataBlock}\n')
                    if dataBlock != b'}ch':
                        # logging.debug(f'measurement: index {index} elapsed time = {self.runTimer.nsecsElapsed()/1e6}')
                        c, data = struct.unpack('<' + 'cH', dataBlock)
                        logging.debug(f'measurement: dataBlock: {dataBlock} data: {data}\n')
                        readings[scanID, index] = (data / 32) - self.scale  # scale 0..4095 -> -128..-0.03 dBm
                        index += 1
                        
                        self.printProgress(index, self.points, f'{scanID+1}/{maxCount}:')
                    logging.debug(f'measurement: level = {(data / 32) - self.scale}dBm')
                self.usb.read(2)  # discard the command prompt
                scanID += 1
                if scanID == maxCount:
                    self.sweeping = False
                    print()
            except serial.SerialException:
                logging.info('serial port exception')
                self.sweeping = False
        
    def sweepTimeout(self):  # freqs are in Hz
        timeout = ((self.endF - self.startF) / 20e3) / (self.rbw ** 2) + self.points / 500
        timeout = timeout * 20 / self.points + 1  # minimum is 1 second
        logging.debug(f'sweepTimeout = {timeout:.2f} s')
        return timeout