# Script for automatic measurement for EMC pre-certification
#
# Used Equipment:
#   - TinySA Ultra
#   - LISN (9 kHz - ~200 MHz) with attenuator of 10 dB and transient limiter
#       https://github.com/bvernoux/EMC_5uH_LISN
#   - DM/CM separator
#
# Script is based on QtTinySA (USB reading part)
#  https://github.com/g4ixt/QtTinySA
#
# Levels are only for information purpose and do not correspond with
#  measurement on 100%, but it is great approximation

import USB_Device
import logging
import matplotlib.pyplot as plt
import numpy as np
import csv

logging.basicConfig(format="%(message)s", level=logging.INFO)

device = USB_Device.USB_Device()
measureAverages = 4

# Conversion from dBm to dBV and its approximation of EMC measurement in test chamber
    # 107 dB - dBm_to_dBV
    # 10 dB  - average attenuation of LISN and cables
dBm_to_dbV = 117        

outputFileName = f"F0.15-1000MHz_avg{measureAverages}.csv"

# in MHz
# approximated limits!
limits = [[0,    230, 230, 1000], [40.5, 40.5,  47.7,  47.5]]     # CISPR32 Class A @ 3m

def measureRange(start, end, rbw, count):
    device.setRBW(rbw)
    frequencies = device.set_frequencies(start, end)
    readings = np.full((measureAverages, device.points), -100, dtype=float)
    device.usbSend()     
    device.measurement(frequencies, readings, count)
    return [frequencies, readings]

def saveAllData(fileName, data, newFile = True):
    header = []
    header.append("Frequency (Hz)")
    header.append("Average (dBm)")
    header.append("Min (dBm)")
    header.append("Max (dBm)")
    for i in range(0, len(data[1])):
        header.append(f"Data {i+1} (dBm)")
    fileAccess = "a"
    if newFile:
        fileAccess = "w"
    with open(fileName, fileAccess) as fileOutput:
        output = csv.writer(fileOutput, delimiter=',', lineterminator="\n")
        if newFile:
            output.writerow(header)
        for i in range(1, len(data[0])):
            avg = 0
            max = data[1][0][i]
            min = data[1][0][i]
            for j in range(0, len(data[1])):
                avg = avg + data[1][j][i]
                if data[1][j][i] > max:
                    max = data[1][j][i]
                if data[1][j][i] < min:
                    min = data[1][j][i]
                    
            
            row = [data[0][i], avg/len(data[1]), min, max]
            for j in range(0, len(data[1])):
                row.append(data[1][j][i])
            
            output.writerow(row)

def readCSV(filename):
    with open(filename, "r") as fileInput:
        input = csv.reader(fileInput, delimiter=',')
        line_count = 0
        data = {}
        data["F"] = []
        data["RAW"] = []
        data["AVG"] = []
        data["MIN"] = []
        data["MAX"] = []
        
        for row in input:
            if line_count == 0:
                # print(f'Column names are {", ".join(row)}')
                line_count += 1
                for i in range(4, len(row)):
                    data["RAW"].append([])
            else:
                # print(f'\t{row[0]} works in the {row[1]} department, and was born in {row[2]}.')
                line_count += 1
                data["F"].append(float(row[0]))
                data["AVG"].append(float(row[1])+dBm_to_dbV)
                data["MIN"].append(float(row[2])+dBm_to_dbV)
                data["MAX"].append(float(row[3])+dBm_to_dbV)
                for i in range(4, len(row)):
                    id = i-4
                    data["RAW"][id].append(float(row[i])+dBm_to_dbV)
                    
    return data

device.openPort()

## Measure all ranges
# 150 kHz - 1 MHz with RBW of 1 kHz
# data = measureRange(150e3, 1e6, 1, measureAverages)
# saveAllData(outputFileName, data)

# 150 kHz - 30 MHz with RBW of 10 kHz (should be 9 kHz according to standard)
data = measureRange(150e3, 30e6, 10, measureAverages)
saveAllData(outputFileName, data)

# 30 MHz - 1 GHz with RBW of 100kHz (should be 120kHz according to standard)
data = measureRange(30e6, 1e9, 100, measureAverages)
saveAllData(outputFileName, data, False)

## Plot data
data = readCSV(outputFileName)
fig, ax = plt.subplots(figsize=(10,7))

freq = np.array(data["F"])/1e6
ax.plot(freq, data["MIN"], label='Min', linewidth=1.0, linestyle = '-')
ax.plot(freq, data["MAX"], label='Max', linewidth=1.0, linestyle = '-')
ax.plot(freq, data["AVG"], label='Average', linewidth=1.0, linestyle = '-')
ax.plot(limits[0], limits[1], label='Limits', linewidth=1.0, linestyle = '-')
ax.legend()
ax.set_xlabel("Frequency (MHz)")
ax.set_ylabel("Magnitude (dBV)")
ax.grid(True)	
ax.set_xscale("log")	
ax.grid(which='minor', linestyle='--', alpha=0.2)
ax.minorticks_on()
ax.set_ylim(bottom=-30, top=60)
ax.set_xlim(left=0.150, right=1000)
fig.show()
fig.savefig(f'{outputFileName}.png', format='png', dpi=600, bbox_inches="tight")

logging.debug("Closing device")
device.closePort()
